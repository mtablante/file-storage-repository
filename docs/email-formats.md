# Email Formats
> For corrections, please inform Miko <jose.suriajr@metrobank.com.ph>

### End of Week (EOW)
- Subject: [For Approval][RMT Entries week ending MMMM DD, YYYY]
- No need to send EOD, since it's included EOW
- CC Miko and Avanna
- Send to Sir Ryan

### End of Day (EOD)
- Subject: [DAT EoD Report][MMDDYYYY]
- CC Miko and Avanna
- Send to Sir Ryan

### DTR
- Subject: [For Approval] DTR of Your Employee Name - Employee ID - Cutoff Date in DTR - MBTC
- CC timesheet, Avanna
- Sent to Miko
- Miko endorses to Sir Ryan

### VL:
- CC timesheet, send to Miko
- Miko endorses to Sir Ryan

### SL:
- Send to Miko, cc timesheet, Avanna
- Miko endorses to Sir Ryan


